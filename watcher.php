<?php
require "RelayPhp.php";

use RelayPHP\Relay;

$relay = new Relay("/dev/ttyUSB0", 0);

function post($url, $data){
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec ($ch);

    curl_close ($ch);
    
    return json_decode($server_output, true);
}

$apiKey = "q1w2e3r4";
$endPoint = "https://visualengine.eu/remote/api/public/";

function isSystemArmed(){
    global $apiKey;
    global $endPoint;
    $data = ['apiKey' => $apiKey];
    $url = $endPoint.'is-system-armed';
    $result = post($url, $data);
    if(array_key_exists('result', $result)){
        return $result['result'] ? 1 : 0;
    }
    return -1;
}
function isInHomeRange(){
    global $apiKey;
    global $endPoint;
    $data = ['apiKey' => $apiKey];
    $url = $endPoint.'is-in-home-range';
    $result = post($url, $data);
    if(array_key_exists('result', $result)){
        return $result['result'] ? 1 : 0;
    }
    return -1;
}

$lastState = false;
while(true){
    $isArmed = isSystemArmed();
    $isInRange = isInHomeRange();
    if($isArmed >= 0 && $isInRange >= 0){
        
        $state = $isArmed == 1 && $isInRange == 1;
        echo "IsArmed $isArmed IsInRange $isInRange\n";
        if($state != $lastState){
            echo "Setting to $state\n";
            $relay->setEnabled($state);
            $lastState = $state;
        }
    }
    sleep(1);
}