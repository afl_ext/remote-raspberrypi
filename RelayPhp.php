<?php
namespace RelayPHP;

class Relay
{
    private $device;
    private $id;
    public function __construct($device, $id){
        $this->device = $device;
        $this->id = $id;
    }

    public function setEnabled($value){
        if($value){
            $this->executeCommand('on');
        } else {
            $this->executeCommand('off');
        }
    }

    private function executeCommand($command){
        exec('mono Relay.exe '.$this->device.' '.$this->id.' '.$command);
    }
}
